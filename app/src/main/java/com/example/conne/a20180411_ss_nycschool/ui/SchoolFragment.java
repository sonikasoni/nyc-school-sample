package com.example.conne.a20180411_ss_nycschool.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.conne.a20180411_ss_nycschool.R;
import com.example.conne.a20180411_ss_nycschool.api.ApiClient;
import com.example.conne.a20180411_ss_nycschool.databinding.SchoolFragmentBinding;
import com.example.conne.a20180411_ss_nycschool.listener.SchoolsResponseListener;
import com.example.conne.a20180411_ss_nycschool.model.School;
import com.example.conne.a20180411_ss_nycschool.ui.schoolDetail.SchoolDetailFragment;
import com.example.conne.a20180411_ss_nycschool.utils.DialogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment for the showing the list of schools
 */
public class SchoolFragment extends Fragment implements SchoolsResponseListener, HomeListCallback {

    private static final String TAG = MainActivity.class.getSimpleName();

    private ApiClient apiClient;
    private SchoolListAdapter adapter;
    private SchoolFragmentBinding binding;
    private List<School> schoolList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.school_fragment, container, false);
        binding = DataBindingUtil.bind(view);
        getActivity().setTitle(R.string.school_fragment);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponents();
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((schoolList == null) || (schoolList.size() == 0)) {
            apiClient.getSchools(this);
            DialogUtil.showDialog(getActivity());
        } else if (adapter != null) {
            binding.recyclerView.setAdapter(adapter);
        }
    }

    /**
     * Initialize components
     */
    private void initComponents() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(linearLayoutManager);
        apiClient = ApiClient.getInstance();

    }

    @Override
    public void getResponse(Object response) {
        DialogUtil.dismissDialog();
        schoolList = (List<School>) response;
        adapter = new SchoolListAdapter(this, schoolList, this);
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void getError(Throwable e) {
        Log.d(TAG, "MainActivity::onFailure = " + e.toString());
    }

    @Override
    public void onItemSelected(School school) {
        if (getActivity() == null || school == null) {
            Toast.makeText(getContext(), getString(R.string.error_message), Toast.LENGTH_LONG).show();
            return;
        }

        ((MainActivity) getActivity()).addFragment(SchoolDetailFragment.getInstance(school.getDbn(), school.getSchoolEmail(), school.getSchoolWebsite()), getResources().getString(R.string.school_detail_fragment));
    }

}
