package com.example.conne.a20180411_ss_nycschool.api;

import com.example.conne.a20180411_ss_nycschool.BuildConfig;
import com.example.conne.a20180411_ss_nycschool.listener.SchoolsResponseListener;
import com.example.conne.a20180411_ss_nycschool.model.School;
import com.example.conne.a20180411_ss_nycschool.model.SchoolInfo;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static ApiClient apiClient;
    private Retrofit retrofit;

    private ApiClient() {
        initClient();
    }

    public static ApiClient getInstance() {
        if (apiClient == null) {
            apiClient = new ApiClient();
        }
        return apiClient;
    }

    /**
     * Method to initialize networking libraries
     */
    private void initClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder().retryOnConnectionFailure(true);
        httpClientBuilder.addInterceptor(logging);

        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.END_POINT).build();
    }

    /**
     * Method to get the list of schools in newyork city
     *
     * @param responseListener
     */
    public void getSchools(final SchoolsResponseListener responseListener) {
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<List<School>> call = service.getSchools();

        call.enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, retrofit2.Response<List<School>> response) {
                responseListener.getResponse(response.body());
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                responseListener.getError(t);
            }
        });
    }

    /**
     * Method to get the SAT test score in a peticular school
     *
     * @param schoolInfoListener
     * @param dbn
     */
    public void getSchoolDetailInfo(final SchoolsResponseListener schoolInfoListener, String dbn) {
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<List<SchoolInfo>> schoolInfoCall = service.getSchoolInfo(dbn);
        schoolInfoCall.enqueue(new Callback<List<SchoolInfo>>() {
            @Override
            public void onResponse(Call<List<SchoolInfo>> call, Response<List<SchoolInfo>> response) {
                if (response.body() == null || response.body().isEmpty()) {
                    schoolInfoListener.getError(new IllegalStateException());
                    return;
                }
                schoolInfoListener.getResponse(response.body().get(0));
            }

            @Override
            public void onFailure(Call<List<SchoolInfo>> call, Throwable t) {
                t.printStackTrace();
                schoolInfoListener.getError(t);
            }
        });
    }
}
