package com.example.conne.a20180411_ss_nycschool.ui;

import com.example.conne.a20180411_ss_nycschool.model.School;

/**
 * Callback for home screen list item click
 */
public interface HomeListCallback {
    void onItemSelected(School school);
}
